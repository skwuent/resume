** Nicholas Gene "Klaus" Sternhagen 
Seeking an integrated DevOps experience working on infrastructure, product code,
and business domain. I want to believe. Particular interest in topics of
internet ifrastructure, energy, sustainability, and 'internet of things'.
Located in San Francisco, with strong preference for positions in that city or
(even better) 100% remote arrangements.

*** Technical skills

**** Vagrant
I started using Vagrant in 2013. I have maintained standard base development
environments with Vagrant at three of my employers.
I also develop an Emacs package related to Vagrant.

**** Salt
I started using Salt in 2015. All internal infrastructure at Lucid was managed
with Salt, and we were in the process of migrating production systems to Salt
when I left.

**** Chef
I currently maintain around 500 AWS and on-prem VMWare hosts using Chef. I
started using Chef in 2016.

**** AWS
At my current employer, BabyCenter, we use some of the most popular AWS services
(EC2, ELB, autoscaling groups) with a combination of CloudFormation and some
wrapper code that we wrote in Ruby.

**** Docker
I have been using Docker at work for production systems and development
infrastructure since 2014.

**** Jenkins
I have worked with Jenkins continuously since 2013

*** Work Experience

**** DevOps Engineer, BabyCenter, Aug 2016-present
BabyCenter services are hit by around 40 million unique users a month.
About 5 million users have installed BabyCenter iOS and Android applications.
At BabyCenter I support production systems and internal infrastructure with
particular forcus on mobile build infrastructure, disaster recovery, and
application monitoring.

**** Engineer, Lucid, June 2015-July 2016
I was hired as the lead QA automation engineer at Lucid, but infrastructure
required to perform these responsibilities was not yet in place. I concentrated
on building out test and CI infrastructure with Salt during my tenure at Lucid.

**** QA Engineer, Riverbed, February 2014-June 2015
Riverbed QA automates a variety of hardware and VM resources arranged to mimic
the 'hybrid networks' of large enterprise customers. This includes testing of
browser UIs, REST apis, and performance.

**** QA Engineer, Telesage, February 2013-February 2014
Telesage develops tools for social science research and IVR.
At Telesage I introduced automated testing and CI practices, with responsibility
for Selenium automation and supporting infrastructure.

*** Education

**** B.A. Oberlin College, 2009
concentration in Chinese language and literature

**** B.Mus. Oberlin College, 2009
concentration in experimental, electronic, and non-Western music
